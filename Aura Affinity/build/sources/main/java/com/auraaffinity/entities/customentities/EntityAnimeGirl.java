package com.auraaffinity.entities.customentities;

import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;

public class EntityAnimeGirl extends EntityCow {

    public EntityAnimeGirl(World world) {
        super(world);
        this.setSize(0.6F, 1.95F);
    }

    @Override
    public EntityCow createChild(EntityAgeable ageable) {
        return new EntityAnimeGirl(world);
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return super.getHurtSound(damageSourceIn);
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return super.getAmbientSound();
    }

    @Override
    protected SoundEvent getDeathSound() {
        return super.getDeathSound();
    }

    @Override
    public float getEyeHeight() {
        return 1.74F;
    }
}
