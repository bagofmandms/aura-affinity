package com.auraaffinity.entities;

import com.auraaffinity.Main;
import com.auraaffinity.util.Reference;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.EntityRegistry;

public class EntityRegistryBase {
    public EntityRegistryBase(String name, Class<? extends Entity> entity, int id, int range, int color1, int color2){
        System.out.println("yotett");
        registerEntity(name, entity, id, range, color1, color2);
    }
    private static void registerEntity(String name, Class<? extends Entity> entity, int id, int range, int color1, int color2){
        EntityRegistry.registerModEntity(new ResourceLocation(Reference.MOD_ID + ":" + name), entity, name, id, Main.instance, range, 1, true, color1, color2);
    }
}
