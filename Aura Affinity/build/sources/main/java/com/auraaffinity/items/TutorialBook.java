package com.auraaffinity.items;

import com.auraaffinity.Main;
import com.auraaffinity.init.modguis.TutorialBookGui;
import com.auraaffinity.init.ModItems;
import com.auraaffinity.util.IHasModel;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class TutorialBook extends Item implements IHasModel{

    public TutorialBook(String name){
        setUnlocalizedName(name);
        setRegistryName(name);
        setCreativeTab(CreativeTabs.DECORATIONS);

        ModItems.ITEMS.add(this);

    }

    public TutorialBook(String name, CreativeTabs creativeTabs){
        setUnlocalizedName(name);
        setRegistryName(name);
        setCreativeTab(creativeTabs);

        ModItems.ITEMS.add(this);

    }
    public void registerModels(){

        Main.proxy.registerItemRenderer(this, 0, "inventory");
    }


    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer entityplayer, EnumHand hand) {
        if (world.isRemote) {
            Minecraft.getMinecraft().displayGuiScreen(new TutorialBookGui());
        }
        return super.onItemRightClick(world, entityplayer, hand);
    }
}
