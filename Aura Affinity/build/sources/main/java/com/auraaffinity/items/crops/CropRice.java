package com.auraaffinity.items.crops;

import com.auraaffinity.init.ModBlocks;
import net.minecraft.block.state.IBlockState;

public class CropRice extends CropBase {

    public CropRice(){
        super("rice");
    }

    @Override
    IBlockState getIBlockState() {
        return ModBlocks.RICE_PLANT.getDefaultState();
    }
}
