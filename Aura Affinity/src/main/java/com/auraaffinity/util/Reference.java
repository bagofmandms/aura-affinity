package com.auraaffinity.util;

public class Reference {

    public static final String MOD_ID = "aaura";
    public static final String NAME = "Aura Affinity";
    public static final String VERSION = "0.1";
    public static final String ACCEPTED_VERSIONS = "[1.12.2]";
    public static final String CLIENT_PROXY_CLASS = "com.auraaffinity.proxy.ClientProxy";
    public static final String COMMON_PROXY_CLASS = "com.auraaffinity.proxy.CommonProxy";
    public static final int GUI_FUSION_ALTAR = 0;

    public static final int ENTITY_ANIME_GIRL = 120;
    public static final int ENTITY_FLAMY = 240;
    public static final int ENTITY_PEBBLE = 360;


}
