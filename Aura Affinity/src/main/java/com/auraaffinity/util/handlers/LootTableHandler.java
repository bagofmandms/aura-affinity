package com.auraaffinity.util.handlers;

import com.auraaffinity.util.Reference;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootTableList;

public class LootTableHandler
{
    public static final ResourceLocation FLAMY = LootTableList.register(new ResourceLocation(Reference.MOD_ID, "flamy"));
    public static final ResourceLocation PEBBLE = LootTableList.register(new ResourceLocation(Reference.MOD_ID, "pebble"));
}