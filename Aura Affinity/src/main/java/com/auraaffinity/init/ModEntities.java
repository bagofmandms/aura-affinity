package com.auraaffinity.init;

import com.auraaffinity.Main;
import com.auraaffinity.entities.customentities.EntityFlamy;
import com.auraaffinity.entities.customentities.EntityAnimeGirl;
import com.auraaffinity.entities.customentities.EntityPebble;
import com.auraaffinity.util.Reference;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.EntityRegistry;

public class ModEntities {
    public static void registerEntities() {
        registerEntity("flamy", EntityFlamy.class, Reference.ENTITY_FLAMY, 50, 16777181, 2544047);
        registerEntity("pebble", EntityPebble.class, Reference.ENTITY_PEBBLE, 50, 16777181, 2544047);
        registerEntity("anime_girl", EntityAnimeGirl.class, Reference.ENTITY_ANIME_GIRL, 50, 16777181, 2544047);
    }

    private static void registerEntity(String name, Class<? extends Entity> entity, int id, int range, int color1, int color2) {
        EntityRegistry.registerModEntity(new ResourceLocation(Reference.MOD_ID + ":" + name), entity, name, id, Main.instance, range, 1, true, color1, color2);
    }
}
