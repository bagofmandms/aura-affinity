package com.auraaffinity.tileentities.testte;

import com.auraaffinity.tileentities.BlockTileEntity;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockTestTE extends BlockTileEntity<TileEntityTestTE> {
    public BlockTestTE(String name) {
        super(name, Material.IRON);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if(!worldIn.isRemote){
            TileEntityTestTE tile = getTileEntity(worldIn, pos);
            if(facing == EnumFacing.UP){
                tile.incrementCount();
            }else if(facing == EnumFacing.DOWN){
                tile.decrementCount();
            }
            playerIn.sendMessage(new TextComponentString("Count: " + tile.getCount()));
        }
        return true;
    }

    @Override
    public Class<TileEntityTestTE> getTileEntityClass() {
        return TileEntityTestTE.class;
    }

    @Nullable
    @Override
    public TileEntityTestTE createTileEntity(World world, IBlockState state) {
        return new TileEntityTestTE();
    }
}
