package com.auraaffinity.entities.customrenderers;

import com.auraaffinity.entities.custommodels.ModelAnimeGirl;
import com.auraaffinity.entities.customentities.EntityAnimeGirl;
import com.auraaffinity.util.Reference;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

public class RenderAnimeGirl extends RenderLiving<EntityAnimeGirl> {

    public static final ResourceLocation TEXTURES = new ResourceLocation(Reference.MOD_ID + ":textures/entities/anime_girl.png");

    public RenderAnimeGirl(RenderManager manager){

        super(manager, new ModelAnimeGirl(), 0.5f );
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityAnimeGirl entity) {
        return TEXTURES;
    }

    @Override
    protected void applyRotations(EntityAnimeGirl entityLiving, float p_77043_2_, float rotationYaw, float partialTicks) {
        super.applyRotations(entityLiving, p_77043_2_, rotationYaw, partialTicks);
    }
}
